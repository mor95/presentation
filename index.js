/**
 * /**
 * @file Archivo que contiene el módulo presentation
 * @namespace index
 * @module presentation
 * @author Christian Arias <cristian1995amr@gmail.com>
 * @requires module:jquery
 * @requires module:lodash
 * @requires module:stage-resize
 * @requires module:sweetalert
 */

const $ = require('jquery');
const _ = require('lodash');
const stageResize = require('stage-resize');
const animatePerSlide = require('animate-per-slide');
// const Hammer      = require('hammerjs');

/**
 * module.exports Contiene la clase.
 * @class module.exports
 */
module.exports = {
    /**
    * @property {object}  currentProps - Las propiedades actuales del módulo
    * @property {object}  currentProps.$slide - El slide actual visible}
    * @property {object}  currentProps.slidesState - Estado y configuración de slides
    */
    currentProps: {
        $slide: null,
        slidesState: {}
    },
    /**
    * @property {array}  directions - Las direcciones disponibles
    */
    directions: [
        'horizontal',
        'vertical',
        'diagonal'
    ],
    /**
     * Función para inicializar slides y navegación.
     * @function
     * @example
     * presentation.init();
     * @borrows addNavigationEventListeners()
     * @borrows addTouchEventListeners()
     * @borrows addStageResizeListeners()
     * @borrows arrangeSlides()
     * @returns {null}
     */
    init: function(){
        //module.exports.addNavigationEventListeners();
        //module.exports.addTouchEventListeners();
        module.exports.addStageResizeListeners();
        module.exports.arrangeSlides();
    },
    /**
     * Función para obtener elemento con ID 'presentation' o false, de no existir en el DOM.
     * @function
     * @example
     * presentation.getPresentationElement();
     * @returns {object} Elemento con ID 'presentation'
     * @returns {false} De no existir dicho elemento
     */
    getPresentationElement: function(){
        const $presentationElement = $('#presentation');
        if(!$presentationElement.length){
            return false;
        }

        return $presentationElement;
    },
    /**
     * Función para obtener slides dentro del elemento retornado por getPresentationElement()
     * @function
     * @example
     * presentation.getPresentationSlides();
     * @borrows getPresentationElement();
     * @returns {object} Contiene todos los slides en la propiedad $slide, y el elemento de presentación en $presentationElement
     * @returns {false} De ser un valor falsy lo obtenido por getPresentationElement()
     */
    getPresentationSlides: function(){
        const $presentationElement = module.exports.getPresentationElement();
        if(!$presentationElement){
            return false;
        }

        return {
            $slides: $presentationElement.children('.slide'),
            $presentationElement: $presentationElement
        }
    },
    /**
     * Función para ordenar slides
     * @function
     * @param {object} args Argumentos para controlar propiedades como spacing
     * @example
     * presentation.arrangeSlides();
     * @borrows getPresentationSlides();
     * @returns {null} 
     */
    arrangeSlides: function(args){
        args = args || {};
        const slides = module.exports.getPresentationSlides();

        if(!slides){
            console.warn('No slides were found in presentation.');
            return;
        }

        var $stage = $('#stage');
        if($stage.length){
            module.exports.currentProps['3d'] = $stage.attr('data-3d') === 'true';
        }

        const $slides = slides.$slides;

        var selectedDirection;

        _.forEach(module.exports.directions, function(v){
            if(slides.$presentationElement.hasClass(v)){
                selectedDirection = v;
            }
        });

        var lastCoords = {
            x: 0,
            y: 0
        };

        module.exports.currentProps.$slide = $slides.eq(0).addClass('active');

        const bodyClass = $slides.eq(0).attr('data-body-class');
        
        if(typeof bodyClass !== 'undefined' && bodyClass.length){
            module.exports.addCurrentSlideClass({
                class: bodyClass
            });
        }
        else{
            module.exports.removeCurrentSlideClass();
        }

        //Se asignan las coordenadas dependiendo de la dirección 
        //en base a la clase añadida al elemento #presentation
        $slides.each(function(){
            $(this).css({
                'left': lastCoords.x + '%',
                'top': lastCoords.y + '%'
            }).data('presentation', module.exports.getDefaultData());
            
            if(selectedDirection === 'horizontal'){
                lastCoords.x += args.spacing || 100;
            }

            if(selectedDirection === 'vertical'){
                lastCoords.y += args.spacing || 100;
            }

            if(selectedDirection === 'diagonal'){
                lastCoords.x += args.spacing || 100;
                lastCoords.y += args.spacing || 100;
            }
        });

        animatePerSlide.setAnimationDurations({
            $stage: $stage
        });

        module.exports.switchToSlide({
            slide: module.exports.currentProps.$slide 
        });
    },
    /**
     * Función para añadir clase de slide actual a body en atributo
     * @function
     * @param {object} args
     * @returns {null} 
     */
    addCurrentSlideClass: function(args){
        if(!args || !args.class || !args.class.length){
            throw new Error('A string is expected.');
        }
        
        $('body').attr({
            'data-current-slide-class': args.class
        });
    },
    /**
     * Función para remover atributo data-current-slide-class de body
     * @function
     * @returns {null} 
     */
    removeCurrentSlideClass: function(){
        $('body').removeAttr('data-current-slide-class');
    },
    addStageResizeListeners: function(){
        const $presentationElement = module.exports.getPresentationElement();
        if($presentationElement && $presentationElement.length){
            var $stage = $('#stage');
            if($stage.length){
                $stage.on('stageResize', function(){
                    /*module.exports.switchToSlide({
                        slide: module.exports.currentProps.$slide
                    });*/
                });
            }
        }  
    },
    addNavigationEventListeners: function(){
        $(document).on({
            'keydown': function(e) {
                if(e.target.tagName.toLowerCase() === 'body'){//If event is fired while nothing is focused
                    var slide;
                    if(e.which === 39 || e.which === 40){
                        e.preventDefault(); // prevent the default action (scroll / move caret)
                        module.exports.switchToSlide({slide:'next'});
                    }
                    if(e.which === 37 || e.which === 38){
                        e.preventDefault(); // prevent the default action (scroll / move caret)
                        module.exports.switchToSlide({slide:'prev'});
                    }
                }
            }
        });
        const $presentationElement = module.exports.getPresentationElement();
        if($presentationElement && $presentationElement.length){
            $presentationElement.on('mousewheel DOMMouseScroll', function(event){
                if(!module.exports.eventTriggeredOnSlide(event)){
                    return true;
                }
                if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
                    module.exports.switchToSlide({slide:'prev'});
                }
                else {
                    module.exports.switchToSlide({slide:'next'});
                }
            });
        }
    },
    eventTriggeredOnSlide: function(event){
        var triggeredOnSlide = false;
        const slides = module.exports.getPresentationSlides();
        if(!slides){
            return;
        }

        slides.$slides.each(function(v){
            var $e = $(this);
            if($(event.target).is($e)){//If the event was started from one of the slides
                triggeredOnSlide = true;
                return true;
            }
        });
    
        return triggeredOnSlide;
    },
    addTouchEventListeners: function(){
        
        const $presentationElement = module.exports.getPresentationElement();
        if($presentationElement && $presentationElement.length){
            var body = document.body;
            var hammertime = new Hammer(body);
            module.exports.currentProps.hammerInstance = hammertime;
            hammertime.get('swipe').set({ direction: Hammer.DIRECTION_ALL });
            hammertime.on('swipe', function(ev) {
                 if(module.exports.eventTriggeredOnSlide(ev)){
                    return false;
                }

                if(ev.direction === 2 || ev.direction === 8){//left - up
                    module.exports.switchToSlide({
                        slide: 'next'
                    });
                }
                if(ev.direction === 4 || ev.direction === 16){//right - down
                    module.exports.switchToSlide({
                        slide: 'prev'
                    });
                }
            });
        }
    },
    switchToSlide: function(args){
        if(module.exports.currentProps.sliding === true){
            // console.warn('---Still sliding.');
            return false;
        }
        if(!args || !args.slide){
            throw new Error('A slide is expected.');
        }

        const slides = module.exports.getPresentationSlides();

        if(!slides){
            console.warn('No slides were found in presentation.');
            return;
        }

        clearTimeout(module.exports.slideHaltTimer);
        module.exports.currentProps.sliding = true;
        
        module.exports.slideHaltTimer = setTimeout(function(){
            module.exports.currentProps.sliding = false;
        }, 500);//0.4s in transition + 100ms
        
        var $targetSlide;


        if(args.slide instanceof jQuery){
            if(!args.slide.length){
                console.error('Invalid slide element', args.slide);
                return;
            }
            if(slides.$presentationElement.has(args.slide).length){
                $targetSlide = args.slide;
            }
        }
        else{
            if(typeof args.slide !== 'string'){
                throw new Error('Invalid slide to switch to. Expected a jQuery element or a string.');
            }
            if(args.slide === 'prev'){
                $targetSlide = module.exports.currentProps.$slide.prev();
            }
            if(args.slide === 'next'){
                $targetSlide = module.exports.currentProps.$slide.next();
            }
            if(args.slide === 'home'){
                if(module.exports.currentProps.$slide.index() === 0){
                    //Already in home
                    return false;
                }
                $targetSlide = module.exports.currentProps.$slide.siblings().eq(0);
            }
            if(!$targetSlide.length){
                // //console.log('Reached the end or the beginning of presentation.');
                return false;
            }
        }

        if($targetSlide.data('presentation').enabled === true){
            module.exports.moveToSlide({
                $slide: $targetSlide,
                $presentationElement: slides.$presentationElement
            });
    
            const bodyClass = $targetSlide.attr('data-body-class');
    
            if(typeof bodyClass !== 'undefined' && bodyClass.length){
                module.exports.addCurrentSlideClass({
                    class: bodyClass
                });
            }
            else{
                module.exports.removeCurrentSlideClass();
            }
        }
    },
    concatTransformProperties: function(args){
        if(!args || !args.transform){
            throw new Error('An object with transform properties was expected.');
        }
        
        var transform = [];
        
        _.forEach(args.transform, function(v, k){
            var unit = k.indexOf('rotate') > -1 ? 'deg' : 'px';
            transform.push(k + '(' + v + unit + ')');
        });

        return transform.join(' ');
    },
    moveToSlide: function(args){
     
        if(!args){
            throw new Error('Missing arguments.');
        }
        if(!args.$slide || !args.$slide.length || !args.slide instanceof jQuery){
            throw new Error('Incorrect $slide to move presentation to.');
        }
        if(!args.$presentationElement || !args.$presentationElement.length || !args.$presentationElement instanceof jQuery){
            throw new Error('Incorrect $presentationElement.');
        }
        
        const position = args.$slide.position();
        const dimensions = {
            width: args.$presentationElement.outerWidth(),
            height: args.$presentationElement.outerHeight()
        };

        module.exports.currentProps.$slide.removeClass('active');

        animatePerSlide.setAnimationClasses({
            $slide: module.exports.currentProps.$slide,
            action: 'remove'
        });

        module.exports.currentProps.$slide = args.$slide;

        const untransformedPosition = {
            x: position.left / stageResize.currentProps.scale,
            y: position.top / stageResize.currentProps.scale
        }

        const coords = {
            x: untransformedPosition.x * -1,
            y: untransformedPosition.y * -1,
        }

        const presentationElementCoords = {
            x: parseInt(args.$presentationElement.css('left')) * -1,
            y: parseInt(args.$presentationElement.css('top')) * -1,
        }

        // //console.log('coords', coords);
        // //console.log('dimensions', dimensions);
        // //console.log('position', position);
        // //console.log('currentProps', stageResize.currentProps);

        args.$slide.addClass('active');

        var cssRules = {};

        
        if(module.exports.currentProps['3d'] === true){
            const $stage = $('#stage');

            var transform = {
                translateX: coords.x + presentationElementCoords.x,
                translateY: coords.y + presentationElementCoords.y
            }

            var last3dTransform = module.exports.currentProps['3dTransform'];

            // //console.log('transform', transform, 'last3dTransform', last3dTransform);
            if(typeof last3dTransform !== 'undefined'){

                // //console.log('TRANSFORM VALUES', transform.translateX, last3dTransform.translateX);
                // //console.log('TRANSFORM VALUES', transform.translateY, last3dTransform.translateY);
                if(transform.translateX !== last3dTransform.translateX){
                    // //console.log('IN CONDITION');
                    if(transform.translateX > last3dTransform.translateX){
                        transform.rotateY = 30;
                    }
                    else{
                        // transform.rotateY = -30;
                        transform.rotateY = 30;
                    }
                }

                if(transform.translateY !== last3dTransform.translateY){
                    // //console.log('IN CONDITION');
                    if(transform.translateY > last3dTransform.translateY){
                        transform.rotateX = -30;
                    }
                    else{
                        transform.rotateX = -30;
                        // transform.rotateX = -30;
                    }
                }
                
            }


           cssRules = {
                transform: module.exports.concatTransformProperties({
                    transform: transform
                })
            }

            module.exports.currentProps['3dTransform'] = transform;

            clearTimeout(module.exports.slideHaltTimer);
            
            
            
            setTimeout(function(){
                var newTransform = _.merge(
                    transform,
                    {
                        rotateX: 0,
                        rotateY: 0
                    }
                );
                
                var cssRules = module.exports.concatTransformProperties(
                    {
                        transform: newTransform
                    }
                );
                
                //console.log('Setting new transform', newTransform);
                args.$presentationElement.css({
                    transform: cssRules
                })
                
                module.exports.currentProps['3dTransform'] = newTransform;
                module.exports.slideHaltTimer = setTimeout(function(){
                    module.exports.currentProps.sliding = false;
                }, 460);//0.4s in transition + 60ms
            }, 450);
        }
        else{
            _.merge(cssRules, {
                left: coords.x + 'px',
                top: coords.y + 'px'
            });
        }
        
        // //console.log('cssRules', cssRules);
        module.exports.currentProps.cssRules = cssRules;

        animatePerSlide.setAnimationClasses({
            $slide: args.$slide,
            action: 'add'
        });
        
        return args.$presentationElement.css(cssRules).trigger('slide', [{
            $element: args.$slide,
            index: args.$slide.index(),
            dimensions: dimensions,
            coords: coords
        
        }]);
    },
    getDefaultData: function(){
        return {
            enabled: true
        }
    },
};